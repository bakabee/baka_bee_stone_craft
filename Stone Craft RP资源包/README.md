
###  **欢迎来到** 
###  **石头工艺** 
![输入图片说明](%E8%AF%B7%E5%BF%BD%E7%95%A5%E6%AD%A4%E6%96%87%E4%BB%B6%E5%A4%B9/main_iogo.png)

##  **下载** 
- gitee下载：
  点击右上![右上角](%E8%AF%B7%E5%BF%BD%E7%95%A5%E6%AD%A4%E6%96%87%E4%BB%B6%E5%A4%B9/image1.png)点击![zip](%E8%AF%B7%E5%BF%BD%E7%95%A5%E6%AD%A4%E6%96%87%E4%BB%B6%E5%A4%B9/image.png)，完成机器验证即可
- [点我下载](https://get-attach.klpbbs.com:11179/getFile.php?size=1868230&from=28013&l=EUalBUkKvK34Q2MVeAm8Jjre0ropNhTUkLOpzKI0BsKoEOwCOJEYwkJlneTHTJWNaUCxxjzS9rUOILPY%2BYw1GQ%3D%3D&n=Stone+Craft+Beta+V0.8.mcaddon)
- 云盘：[点我下载](https://nsub2d3.118pan.com/dl.php?MzA3MkFvZitsYlJQVnQzenN6RHhnQkVFZHV0cUprL0wrdWZNczVXQ3NKMUpHNjRtTGxQc2dPajRYV2pvSkZMNklDVVFGL29QeVBCeTVCNVpTY25zSUt1WjFGMW9WczJ3US91NE0xbjkzU2IvVjhsQ1lQQkNXaEI5bDlnUjFrazFVWnFDL0FFaUw3V2h0cE14UGlLV3NuOG1BU3RpRTExTE1wRWRQU2E4aG41czBQcW9NaDB3bFhPSCtYSmZwWjlVdTJPZ1VBZlBHNjUrTUVFaEN4eDVLY3MrZWZLTXJsalBZWGo1M1dmVmVsUWRjVjk4QzN2U3pLUmNkYUtKQ3BUeE1neXJiay83V0d4NU9BNE5aRDJGU29tWUNR)

## 这是什么？
『石头工艺』，一适用于Minecraft基岩版的附加包（即所谓“游戏模组”），其完全按照MIT授权协议提供，在不断的更新下，将逐渐成为一款功能强大的附加包。
在这里，你可以通过不断 **挖掘石头** 或 **击败石头生物** 获得 **压缩石头装备** ，最后击败 **石头巨人** 。
具体内容请前往[这里](https://klpbbs.com/thread-28013-1-1.html)查阅
###  **想要正确安装本附加包，需要您具备以下能力：** 


- 一个可以正常下载文件的浏览器
- Minecraft基岩版
- 有一定的能力导入附加包

### 如果你具备以下的能力，就可以对本包进行修改：

- 有制作附加包的经验

### 除此之外，请确定您的游戏版本是否符合要求，否则可能产生兼容性问题：

```yaml
Minecraft基岩版: 1.17.0 ~ 最新
推荐的版本： 最新正式版 #测试版与预览版也可使用，不过未经测试
最低版本： 1.17.0 #可能导致部分物品、配方无法加载
```

## 实验性玩法
请打开以下实验性玩法以保证附加包的正常运行：


- 必须：
  - 假日创造者功能
  - 自定义生物群系
  - Molang 功能


可选：
- 下一个主要更新

### 特别感谢：
- @方漓猫 提供的README模板
## 许可协议


本项目是由  @BakaBee  开发并维护,使用 MIT 许可协议进行授权，拷贝、分享或基于此进行创作时请遵守协议内容：


> Copyright © 2023 BakaBee
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

## 写在最后
做附加包不容易，如果可以，请到[苦力怕论他](https://klpbbs.com/thread-28013-1-1.html)支持一下我～